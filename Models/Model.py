import torch.nn as nn
import torch.nn.functional as F
import torch
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random
import tqdm
from torch.utils.data import DataLoader
import pickle


## define NN architecture
class MLP_KG(nn.Module):
    def __init__(self, tot_entity, tot_relation, emb_size, hidden_size):
        ##
        super(MLP_KG, self).__init__()
        ##
        self.ent_embeddings = nn.Embedding(tot_entity, emb_size)
        self.rel_embeddings = nn.Embedding(tot_relation, emb_size)
        nn.init.xavier_uniform_(self.ent_embeddings.weight)
        nn.init.xavier_uniform_(self.rel_embeddings.weight)
        ##
        self.M1 = nn.Linear(emb_size, hidden_size, bias = False)
        self.M2 = nn.Linear(emb_size, hidden_size, bias = False)
        self.M3 = nn.Linear(emb_size, hidden_size, bias = False)
        ##
        self.hidden_fc = nn.Linear(hidden_size, int(hidden_size/2))
        ##
        self.hidden_fc_2 = nn.Linear(int(hidden_size/2), int(hidden_size/4))
        ##
        self.hidden_fc_3 = nn.Linear(int(hidden_size/4), 1)
        ##
        # dropout layer (p=0.2)
        # dropout prevents overfitting of data
        self.droput = nn.Dropout(0.2)
        ##        
    def forward(self, h, r, t):
        # add hidden layer, with relu activation function
        #
        x_h = self.ent_embeddings(h)
        x_r = self.rel_embeddings(r)
        x_t = self.ent_embeddings(t)
        ##
        Tanh = torch.tanh(self.M1(x_h) + self.M2(x_r) + self.M3(x_t))
        # add dropout layer
        Tanh = self.droput(Tanh)
        #
        fc1 = self.hidden_fc(Tanh)
        #
        fc1 = torch.tanh(fc1)
        #
        fc1 = self.droput(fc1)
        #
        fc2 = self.hidden_fc_2(fc1)
        #
        fc2 = torch.tanh(fc2)
        #
        fc2 = self.droput(fc2)
        #
        z = self.hidden_fc_3(fc2)
        #
        #y_pred = torch.sigmoid(z)
        return z