# Travel Knowledge Graph Embeddings for Recommendation (TKE)

A python implementation of TKE framework proposed in [Optimizing Email Marketing Campaigns in the Airline Industry using Knowledge Graph Embeddings](https://www.eurecom.fr/en/publication/6511). Submitted to the International Workshop on Product Knowledge Graph for Ecommerce ([PKG4Ecommerce'21](https://km-ecomm-www.github.io/2021/)). 

![TKE](Images/TKE_AAM.png)

# Software dependencies

This project has a number of software dependencies, listed in the requirements.txt file: 

numpy==1.19.2 
pandas==0.24.1
pykg2vec==0.0.51
scikit-learn==0.24
scipy==1.5.4
torch==1.7.1
torchvision==0.8.2
tqdm==4.54.1
xgboost==1.3.3


# Directories structure 

|    Directory    |                         Content                                |
|-----------------|----------------------------------------------------------------|
|[src](https://gitlab.eurecom.fr/amadeus/tke4rec/-/tree/master/src)	  | scripts to learn KG Embeddings, train XGBoost Model    |             
|[Utils](https://gitlab.eurecom.fr/amadeus/tke4rec/-/tree/master/Utils)             | training data used for models,airports data for matching       |
|[Models](https://gitlab.eurecom.fr/amadeus/tke4rec/-/tree/master/Models)		  | module that contain all implemented models             |
|[Notebooks](https://gitlab.eurecom.fr/amadeus/tke4rec/-/tree/master/Notebooks)          | Notebooks that are used to explore models                    |
|[ontology](https://gitlab.eurecom.fr/amadeus/tke4rec/-/tree/master/ontology)          | Folder that contains the travel ontology used to create the KG        |

## Dataset
The dataset used in the paper is not available for privacy and GDPR reasons. We believe that our approach TKE can be used for other datasets (which will be one of our future work). We consider though that it is worth publishing the source code that allowed us to run the experiments and obtain the results presented in the paper. 

## Learning Embeddings
### MLP Algorithm
One could use MLP_Train.py script to train and evaluate at the same time the MLP algorithm. You can choose the hyper-parameters based on argument list provided in the file.

Run MLP:
```
#!/usr/bin/env bash
python MLP_Train.py --epochs 100 --Ns 4 --embedding_size 128 --batch_size 128 --lr 0.003
```

### TransX Algorithms 
Please refer to the documentation provided in [pykg2vec repository](https://github.com/Sujit-O/pykg2vec). The dataset used should be in the format of triples (h,r,t), one should have three files for each dataset (train, val, test).

Run TransE:
```
#!/usr/bin/env bash
pykg2vec-train -mn TransE -device cuda -k 100 -lr 0.03 -l 100 -b 512 -ds [name] -dsp [path to the custom dataset]
```

# Extracting KG Embeddings
Once the KG Embedding algorithms are trained and evaluated, embeddings are generated and contained either in dataset folders (in the case of transX algorithms) or obtained by calling the model attributes (in the case of MLP algorithm).
Please refer to this [Notebook](https://gitlab.eurecom.fr/amadeus/tke4rec/-/blob/master/Notebooks/Embeddings_Extraction.ipynb) for more details.

# Training XGBoost Model 
Once the data is ready and preprocessed to be in the format [features vector, target\], one can use XGB_Train.py file to train XGBoost model with the desired input data. We give below an illustration of the command to run.

Run XGB_Train:
```
#!/usr/bin/env bash
python XGB_Train.py --ds Extra_leg_room --dtype KGE --max_depth 10 --n_trees 100 --colsample 0.8 --subsample 0.75 
```

# Evaluating XGBoost Model
Once XGBoost model trained, you will need to run XGB_Evaluate to get values of the metrics described in the paper. In the folder [Image](), you can have a look to some examples 

Run XGB_Evaluate:
```
#!/usr/bin/env bash
python XGB_Evaluate.py --ds Extra_leg_room --dtype KGE 
```
