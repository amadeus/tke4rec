import torch.nn as nn
import torch.nn.functional as F
import torch
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random
import tqdm
from torch.utils.data import DataLoader
import pickle
import sys
sys.append('../models/')
from Model import MLP
from torch.utils.data import Dataset, TensorDataset
import argparse
import os
os.environ['CUDA_LAUNCH_BLOCKING'] = "1"

#
parser = argparse.ArgumentParser()

parser.add_argument('-ds', type=str, required=False, default="Extra_leg_Room")
parser.add_argument('-epochs', type=int, required=False, default=30)
parser.add_argument('-lr', type=float, required=False, default=0.001)
parser.add_argument('-model_path', type=str, required=False, default='MLP')
parser.add_argument('-emb_size', type=int, required=False, default=64)
parser.add_argument('-batch_size', type=int, required=False, default=128)
parser.add_argument('-hidden_size', type=int, required=False, default=32)
args = parser.parse_args()

## Initialize variable arguments
ds_path = args.ds
epochs = args.epochs
learning_rate = args.lr
model_path = args.model_path
emb_size = args.emb_size
hidden_size = args.hidden_size
batch_size = args.batch_size


# Set device to cuda if available

if torch.cuda.is_available():  
    dev = "cuda:0" 
else:  
    dev = "cpu" 

device = torch.device(dev)

## Load entities & relations dict
with open('../Data/' + ds_path + '/dict_ent_2_idx.pkl', 'rb') as handle:
    dict_ent_2_idx = pickle.load(handle)
with open('../Data/' + ds_path + '/dict_rel_2_idx.pkl', 'rb') as handle:
    dict_rel_2_idx = pickle.load(handle)

## Load train and validation sets (Triples of ids of the KG)
X_train = np.load('../Data/MLP/' + ds_path + '/X_train.npy')
y_train = np.load('../Data/MLP' +ds_path + '/y_train.npy')
##
X_valid = np.load('../Data/MLP' +ds_path + '/X_valid.npy')
y_valid = np.load('../Data/MLP' +ds_path + '/y_valid.npy')

# Now we can create a model and send it at once to the device
model = MLP_KG(len(dict_ent_2_idx), len(dict_rel_2_idx), emb_size, hidden_size)
model.to(device)
# We can also inspect its parameters using its state_dict
print(model)

def make_train_step(model, loss_fn, optimizer):
    # Builds function that performs a step in the train loop
    def train_step(x, y):  
        optimizer.zero_grad()
        outputs = model.forward(x[:, 0], x[:, 1], x[:, 2])
        loss = loss_fn(outputs, y)
        loss.backward()
        optimizer.step()
        return loss.item()
    # Returns the function that will be called inside the train loop
    return train_step

loss_fn = nn.BCEWithLogitsLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
# Creates the train_step function for our model, loss function and optimizer
train_step = make_train_step(model, loss_fn, optimizer)
losses = []

# Train loader
x_train_tensor = torch.from_numpy(X_train)
y_train_tensor = torch.from_numpy(y_train)
train_data = TensorDataset(x_train_tensor, y_train_tensor)
train_loader = DataLoader(dataset=train_data, batch_size=, shuffle=True)

##valid set
x_valid_tensor = torch.from_numpy(X_valid).to(device)
y_valid_tensor = torch.from_numpy(y_valid).to(device)

loss_acc = 10**6
print('Begin the training')
for epoch in tqdm.tqdm(range(epochs)):
    loss_epoch = []
    for x_batch, y_batch in train_loader:
        loss = train_step(x_train_tensor.to(device), y_train_tensor.to(device))
        loss_epoch.append(loss)
        losses.append(loss)
        print('epoch {}, Training loss {}'.format(epoch, np.mean(loss_epoch)))
    with torch.no_grad():
        outputs_valid = model.forward(x_valid_tensor[:, 0], x_valid_tensor[:, 1], x_valid_tensor[:, 2])
        loss_valid = loss_fn(outputs_valid, y_valid_tensor)
        print('epoch {}, Validation loss {}'.format(epoch, loss_valid))
        hit_10, mrr_10, K, pos_score_l, neg_score_l, counter_l = eval_function(dict_test_positive_user2item, dict_occ_ceid, dict_neg_instances, 2, 10)
        print('Hit@10: ', hit_10)
        print('MRR@10: ', mrr_10)

# Save the model        
torch.save(model, '../Models/mlp/' + ds_path + '_' +  model_path +'.pt')