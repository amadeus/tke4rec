import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.metrics import confusion_matrix
from xgboost import XGBClassifier
import sys
sys.path.append('../')
from Utils.utils_func import *
import time
import argparse
from pypmml import Model
import argparse

#
parser = argparse.ArgumentParser()

parser.add_argument('-ds', type=str, required=False, default="Extra_leg_room")
parser.add_argument('-dtype', type=str, required=False, default="KGE")
args = parser.parse_args()

# Load data
X_train=np.load('../Data/' + args.ds + '_' + args.dtype + '/X_train.npy')
y_train=np.load('../Data/' + args.ds + '_' + args.dtype + 'y_train.npy')
X_test=np.load('../Data/' + args.ds + '_' + args.dtype + '/X_test.npy')
y_test=np.load('../Data/' + args.ds + '_' + args.dtype + '/y_test.npy')

## Load the model
xgb_model = Model.fromFile('../Models/XGBoost/' + args.ds + "_pmml.pmml')

#### Training predictions (to demonstrate overfitting)
train_xgb_predictions = xgb_model.predict(X_train)
train_xgb_probs = xgb_model.predict_proba(X_train)[:, 1]
# Actual class predictions
xgb_predictions = xgb_model.predict(X_test)
# Probabilities for each class
start_time = time.clock()
xgb_probs = xgb_model.predict_proba(X_test)[:, 1]
print(time.clock() - start_time, "seconds", " for", len(X_test), " rows")

# Calculate roc auc
roc_value = roc_auc_score(y_test, xgb_probs)
print('Roc Score: ', roc_value)

# Calculate pr auc
pr_auc_score = average_precision_score(y_test, xgb_probs)
print('PR AUC Score: ', pr_auc_score)

# Confusion matrix
cm = confusion_matrix(y_test, xgb_predictions)
plot_confusion_matrix(cm, classes = ['No Sales', 'Sales'],
                      title = 'Confusion Matrix')

## Compute TPRs and CRs evolution
list_p = np.arange(0,1,0.01)
list_CR = list()
list_TPR = list()
list_TNR = list()
for p in list_p:
    ## If we want to change the prob threshold from which we change 
    xgb_predictions = (xgb_model.predict_proba(X_test)[:,1] >= p).astype(bool)
    cm = confusion_matrix(y_test, xgb_predictions)
    ##
    list_TPR.append(round(cm[1,1]*100/(cm[1,1] + cm[1,0]),2))
    list_TNR.append(round(cm[0,0]*100/(cm[0,0] + cm[0,1]),2))
    list_CR.append(round(cm[1,1]*100/((cm[1,1] + cm[0,1])*53),2))
    ##


## create figures of TPR, TNR evolution and axis objects with subplots()
fig,ax = plt.subplots()
# make a plot
ax.plot(list_p, list_TPR, color="blue", marker="o")
# set x-axis label
ax.set_xlabel("p",fontsize=14)
# set y-axis label
ax.set_ylabel("TPR",color="blue",fontsize=14)
##
# twin object for two different y-axis on the sample plot
ax2=ax.twinx()
# make a plot with different y-axis using second axis object
ax2.plot(list_p, list_TNR,color="green",marker="o")
ax2.set_ylabel("TNR",color="green",fontsize=14)
# Register TPR, TNR figures
fig.savefig('../Images/'+ args.ds + '_' + args.dtype + 'TPR_TNR.png',
            format='jpeg',
            dpi=100)   

## create figures of TPR, CR evolution and axis objects with subplots()
fig,ax = plt.subplots()
# make a plot
ax.plot(list_p, list_TPR, color="blue", marker="o")
# set x-axis label
ax.set_xlabel("p",fontsize=14)
# set y-axis label
ax.set_ylabel("TPR",color="blue",fontsize=14)
##
# twin object for two different y-axis on the sample plot
ax2=ax.twinx()
# make a plot with different y-axis using second axis object
ax2.plot(list_p, list_TNR,color="green",marker="o")
ax2.set_ylabel("TNR",color="green",fontsize=14)
plt.show()                 
# Register TPR, CR figures
fig.savefig('../Images/'+ args.ds + '_' + args.dtype + 'TPR_TNR.png',
            format='jpeg',
            dpi=100)                                                  

# Register PR Curve
pr_evaluate_model(xgb_predictions, xgb_probs, train_xgb_predictions, train_xgb_probs, y_train, y_test)
plt.savefig('../Images/'+ args.ds + '_' + args.dtype + 'pr_curve.png')

# Register ROC Curve
evaluate_model(xgb_predictions, xgb_probs, train_xgb_predictions, train_xgb_probs, y_train, y_test)
plt.savefig('../Images/' + args.ds + '_' + args.dtype + roc_auc_curve.png')

