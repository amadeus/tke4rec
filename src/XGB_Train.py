import pandas as pd
import numpy as np
import itertools
from xgboost import XGBClassifier
import sys
sys.path.append('../')
from Utils.utils_func import *
from imblearn.pipeline import Pipeline
from sklearn.pipeline import Pipeline
from nyoka import xgboost_to_pmml
import time
import argparse

#
parser = argparse.ArgumentParser()

parser.add_argument('-ds', type=str, required=False, default="Extra_leg_room")
parser.add_argument('-max_depth', type=int, required=False, default=20)
parser.add_argument('-n_trees', type=int, required=False, default=30)
parser.add_argument('-colsample', type=float, required=False, default=0.85)
parser.add_argument('-subsample', type=float, required=False, default=0.75)
parser.add_argument('-dtype', type=str, required=False, default="KGE")
args = parser.parse_args()

# Load data
X_train=np.load('../Data/' + args.ds + '_' + args.dtype + '/X_train.npy')
y_train=np.load('../Data/' + args.ds + '_' + args.dtype + 'y_train.npy')
X_test=np.load('../Data/' + args.ds + '_' + args.dtype + '/X_test.npy')
y_test=np.load('../Data/' + args.ds + '_' + args.dtype + '/y_test.npy')

# 
features = pd.read_csv('../Data/' + args.ds + '/features.csv')

## Compute the class_weight function
class_weight = (len(Encoded_y_train)-sum(Encoded_y_train))/sum(Encoded_y_train)


## Initialize the pipeline 
pipeline_obj = Pipeline([
    ('Xgbc',XGBClassifier(objective="binary:logistic", max_depth=args.max_depth, n_estimators=args.n_trees, colsample_bytree=args.colsampe, subsample=args.subsample, scale_pos_weight=class_weight)


print('*************** Training XGBoost Model: ******************')
start_time = time.clock()
pipeline_obj.fit(X_train, y_train)
print(time.clock() - start_time, "seconds", " for training data for ", len(y_train), " rows")

xgboost_to_pmml(pipeline_obj, features, 'Y', '../Models/XGBoost/' + args.ds + "_pmml.pmml")
